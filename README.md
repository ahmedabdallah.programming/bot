<h1  align="center"> bot</h1>  <br>


## Table of Contents

-  [Introduction](#introduction)

-  [Technologies](#technologies)

-  [Start App](#start-app)

-  [Run Docker](#run-docker)

-  [Testing](#testing)


## Introduction

This project target is to Build simple RESTful API to create a backend web server that exposes a single endpoint. This endpoint accepts a bot identifier and a visitor written message. It returns a single reply corresponding to the highest predicted intent above the confidence threshold.



## Technologies
- The application is built using:
* Node.js
* Express
* MongoDB
* Mocha and Chai for testing
   

## Start App

To run the application locally:
- Clone the repo
- browse to application directory
- run `npm run start`
- [Optional] send HTTP POST request with empty body to `http://localhost:3000/api/migrations` to create records in the database 

Notes:
- Application uses MongoDB as a database, Please Make sure to change MongoDB connection URI in `.env` file
- I already created endpoint to add records to db



### Run Docker

To run the application using docker:

- Clone the application
- Browse to application directory
- run `docker build --tag=bot .`
- run `docker run bot`

Note:
In case you are connecting to a local instance of MongoDB, Please make sure to configure Docker connection to a local MongoDB instance
  

## Testing
To run tests, browse to project directory and run 
    `npm run test`

To check testing report, browse to app directory and then:
 `test/report/TestReport.html` 
 It should open in your browser

