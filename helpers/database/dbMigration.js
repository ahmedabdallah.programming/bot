const repliesExamples = require('../../intent_reply_examples.json')
const Replies = require('../../models/reply')


async function createReplies(req, res) {
    try {

        const availableReplies = await Replies.find({});
        if (availableReplies.length < 1)
            await Replies.insertMany(repliesExamples)
        res.send(repliesExamples)

    } catch (error) {
        console.log("Migration Error", error);
        throw error;
    }
}

module.exports = {
    createReplies
}