require('dotenv').config()
const express = require('express');
const app = express();
const router = express.Router();
const bodyParser = require('body-parser');
const helmet = require("helmet");
const cors = require('cors')
const mongoose = require('mongoose')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./docs/swagger.json');
const dbMigrations = require('./helpers/database/dbMigration')
const messageController = require('./controllers/messageController')

// parse requests
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ extended: false }));


// Start database connection
mongoose.connect(process.env.dbURI,
    { useNewUrlParser: true, useUnifiedTopology: true }
    , function (err, data) {
        if (err)
            console.log(err)
    });



// security 
app.use(cors())
app.use(helmet());

// disable unnecessary HTTP methods
const allowedMethods = ['GET', 'POST', 'HEAD']
app.use((req, res, next) => {
    if (!allowedMethods.includes(req.method))
        return res.status(405).send('Method Not Allowed')
    return next()
})


//Router
router.post('/message/reply', messageController.getMessageReply);

//API Docs
router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', swaggerUi.setup(swaggerDocument));

// DB Migration, run it to create records in db
router.post('/migrations', dbMigrations.createReplies)


// routes prefix
app.use('/api', router)

// server up
app.listen(process.env.PORT || 3000, () => {
    console.log(new Date().toString(), "Server is on and listening on port ", process.env.PORT || 3000);
});

module.exports = app
