const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const replySchema = new Schema({
    name: {
        type: String,
    },
    description: {
        type: String,
    },
    reply: {
        id: {
            type: String,
        },
        text: {
            type: String,
        }
    }
});


mongoose.model('Replies', replySchema);
module.exports = mongoose.model('Replies');