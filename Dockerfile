# from base image
FROM node:10.15-alpine

# create app directory
RUN mkdir /backend 

RUN chown -R node:node /backend

# copy project files to directory
COPY ./ /backend

# Enter backend dir
WORKDIR /backend

# install modules 
RUN npm install

USER node

# expose port
EXPOSE 3000

# Start server
CMD [ "node", "index.js" ]