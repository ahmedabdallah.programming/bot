const axios = require('axios').default;
const Replies = require('../models/reply')


async function getMessageIntents(message, botId) {
    try {

        const headers = {
            'Content-Type': 'application/json',
            'authorization': process.env.apiAuth
        }
        const messageIntents = await axios.post(`${process.env.apiBaseURL}/intents`, { botId, message }, { headers })
        if (messageIntents.data.intents.length > 0) {
            return messageIntents.data.intents.reduce((prev, current) => (+prev.confidence > +current.confidence) ? prev : current)
        }
        else
            throw 'Failed to get message intent'
    } catch (error) {
        throw error;
    }
}


async function getMessageReply(intent) {
    try {
        const messageReply = await Replies.findOne({ name: intent.name })
        if (messageReply.reply)
            return messageReply.reply.text
        else
            throw 'Failed to get message intent'
    } catch (error) {
        throw error;
    }
}

module.exports = {
    getMessageIntents,
    getMessageReply
}
