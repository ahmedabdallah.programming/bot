const messageService = require('../services/messageService')
const responseBuilder = require('../helpers/response/responseBuilder')

async function getMessageReply(req, res) {
    try {
        const { message, botId } = req.body
        const messageIntent = await messageService.getMessageIntents(message, botId);
        const messageReply = await messageService.getMessageReply(messageIntent);
        responseBuilder.sendResponse('success', '', messageReply, res, 'Reply retrieved successfully')
    } catch (error) {
        console.error('Error in getting message reply', error);
        responseBuilder.sendResponse('failure', 'Server Error, please try again', '', res, 'Failed to retrieve Reply')
    }
}

module.exports = {
    getMessageReply
}