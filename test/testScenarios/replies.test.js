const chai = require('chai');
const expect = require('chai').expect
const app = require('../../index');
const chaiHttp = require('chai-http');

describe("Replies Tests", function () {

    // before(function (done) {
    //     setTimeout(function () {
    //         done();
    //     }, 500);
    // });

    chai.use(chaiHttp);

    it('Create replies in DB', function (done) {
        chai.request(app)
            .post('/api/migrations')
            .set('content-type', 'application/json')
            .end(function (error, response, body) {
                if (error) {
                    console.log(error);
                    done(error);
                } else {
                    expect(response.body).to.be.an('array');
                    response.body.forEach(reply => {
                        expect(reply).to.have.property('id');
                        expect(reply).to.have.property('name');
                        expect(reply).to.have.property('description');
                        expect(reply).to.have.property('reply');
                        expect(reply.reply).to.have.property('id');
                        expect(reply.reply).to.have.property('text');

                    });
                    done();
                }
            });
    })

    it('Get Message Reply', function (done) {
        chai.request(app)
            .post('/api/message/reply')
            .set('content-type', 'application/json')
            .set('authorization', '825765d4-7f8d-4d83-bb03-9d45ac9c27c0')
            .send({
                "botId": "5f74865056d7bb000fcd39ff",
                "message": "Hello"
            })
            .end(function (error, response, body) {
                if (error) {
                    console.log(error);
                    done(error);
                } else {
                    expect(response.status).to.equal(200)
                    expect(response.body.data).to.be.a('string');
                    expect(response.body).to.have.property('status');
                    expect(response.body).to.have.property('message');
                    expect(response.body).to.have.property('data');
                    done();
                }
            });
    })

})